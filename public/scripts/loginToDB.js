// Start listener for chaning log-in status
firebase.auth().onAuthStateChanged(function(user) {
    if(user) {
        console.log("new login");
        // When a user logged in he gets to see the current standings <---- INITIALIZING
        printLeaderboard(comparatorMemberStats_Elo);

    }
    else {
        console.log("Initial listener or sign out.");
    }
});

login(0);

function login(tries) {

    if(tries > 2) {
        alert("You have failed to login. Try again next time.");
        return;
    }

    firebase.auth().signOut().then(function() {
        console.log("Intended sign out.")
    }).catch(function(error) {
        console.log("Error while signing out.")
    });

    var pwd = prompt("What is the name of the color combination green & black?   Tries: " + (3-tries), "");

    if ((pwd != null) || (pwd != "")) {
        console.log("pwd: " + pwd);
        firebase.auth().signInWithEmailAndPassword("geilertyp@kellergang.de", pwd).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log("Error while loggin in: " + error.message);
            console.log("Incorrect PW");
            login(tries + 1);
        });
    }
    else {
        console.log("PW empty or null");
        login(tries + 1);
    }
}