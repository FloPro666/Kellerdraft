/*
 * TODO:
 * Bei bye automatisch ergebnis eintragen
 * Immer dorthin scrollen wo was passiert
 */

let roundsDiv = null;
let roundsTable = null;
const results = ["2:0", "2:1", "1:0", "Draw", "0:1", "1:2", "0:2"];
let eloFactor = 30;
let byePlayer = null;

class Tournament {
    constructor(name, numTeilnehmer) {
        this.name = name;
        this.date = getDate();
        this.numTeilnehmer = numTeilnehmer;
        this.maxRounds = Math.ceil(Math.log2(numTeilnehmer));
        this.roundsPlayed = 0;
        this.currentRound = null;

        byePlayer = new Player(numTeilnehmer, -1, "Bye", 0, numTeilnehmer+1);

        //this.teilnehmerNameMap = idToNameMap;
        //this.teilnehmerEloMap = idToEloMap;

        this.players = new Array(numTeilnehmer);
        this.ranksToPlayers = new Array(numTeilnehmer)

        let i = 0;
        for(let [key, val] of idToNameMap.entries()) {
            this.players[i] = new Player(i, key, val, idToEloMap.get(key), i+1);
            //this.ranksToPlayers[i] = i+1;
            i++;
        }

        // Increment Participations
        let idList = [];
        for(let i = 0; i < this.players.length; i++) {
            idList.push(this.players[i].id);
        }
        incrementParticipations(idList);

        // Randomize starting distribution
        this.players = shuffle(this.players);
        for(i = 0; i < numTeilnehmer; i++) {
            this.players[i].rank = i+1;
        }

        // Initialize the HTML Table-element
        this.initializeTable(this.players);

        // Initialize the HTML Round-table
        this.initializeRounds(this.numTeilnehmer);

        this.currentRound = new Round(this.roundsPlayed, this.maxRounds, this.players);

        this.playRound();
    }

    printRanking(players) {
        for(let i = 0; i < memberTable.rows.length; i++) {
            let row = memberTable.rows[i];
            let player = players[i];
            row.cells[1].innerHTML = player.name;
            row.cells[2].innerHTML = player.numWins;
            row.cells[3].innerHTML = player.numDraws;
            row.cells[4].innerHTML = player.numLoses;
            row.cells[5].innerHTML = player.tb1;
            row.cells[6].innerHTML = player.tb2;
            row.cells[7].innerHTML = player.tb3;
            row.cells[8].innerHTML = player.points;
        }
    }

    initializeTable(players) {
        removeHtmlElement("memberTable");

        memberTable = createTable("memberTableDiv", "memberTable", ["Rank", "Name", "Wins", "Draws", "Loses", "TB1: OMW%", "TB2: PGW%", "TB3: OGW%", "Points"]);


        for(let i = 0; i < players.length; i++) {
            let rowCount = memberTable.rows.length;
            let row = memberTable.insertRow(rowCount);
            row.insertCell(0).innerHTML = (i+1).toString() + '.';
            row.insertCell(1).innerHTML = players[i].name;
            row.insertCell(2).innerHTML = '0';
            row.insertCell(3).innerHTML = '0';
            row.insertCell(4).innerHTML = '0';
            row.insertCell(5).innerHTML = '0';
            row.insertCell(6).innerHTML = '0';
            row.insertCell(7).innerHTML = '0';
            row.insertCell(8).innerHTML = '0';
        }
    }

    initializeRounds() {
        // Create a new div for the pairings
        roundsDiv = document.createElement('div');
        roundsDiv.setAttribute("id", "roundsDiv");

        // Create a new Header for the pairings
        let roundsHeader = document.createElement("h3");
        roundsHeader.setAttribute("id", "roundsHeader");
        roundsDiv.appendChild(roundsHeader);
        roundsHeader.innerHTML = "Round 1";

        // Create a table inside the div
        roundsTable = document.createElement("TABLE");
        roundsTable.setAttribute("id", "roundsTable");
        roundsTable.border = '1';
        roundsDiv.appendChild(roundsTable)

        // Set the basic structure
        roundsTable.createTHead();
        let th0 = roundsTable.tHead.appendChild(document.createElement("th"));
        th0.innerHTML = "Pairings";
        let th1 = roundsTable.tHead.appendChild(document.createElement("th"));
        th1.innerHTML = "Results";
        document.body.appendChild(roundsDiv);

    }

    playRound() {

        // Create rows for the pairings
        let row = null;
        let pairingHasBye = false;
        for(let i = 0; i < this.currentRound.numPairings; i++) {
            let rowCount = roundsTable.rows.length;
            let resultCell = null;

            // Check for bye pairing
            if (this.currentRound.pairings[i].player1 === byePlayer || this.currentRound.pairings[i].player2 === byePlayer) {
                pairingHasBye = true;
            }

            // First Round
            if(this.roundsPlayed === 0) {

                row = roundsTable.insertRow(rowCount);
                row.insertCell(0).innerHTML = this.currentRound.pairings[i].player1.name + "  vs.  " + this.currentRound.pairings[i].player2.name;
                resultCell = row.insertCell(1);
            }
            else {
                row = roundsTable.rows[i];
                row.cells[0].innerHTML = this.currentRound.pairings[i].player1.name + "  vs.  " + this.currentRound.pairings[i].player2.name;
                resultCell = row.cells[1];
                row.cells[1].innerHTML = "";
            }

            // Instantly set result of pairing with Bye
            if(pairingHasBye) {
                this.createCheckboxForm(i, pairingHasBye, resultCell, null)
            }
            else {
                this.createCheckboxForm(i, pairingHasBye, resultCell, null);
            }
        }
    }

    processResult(row) {

        // Get Result
        const resultValue = getSelectedCheckbox("resultCheckbox" + row.toString());
        const resultString = results[resultValue];

        // Set cell input to Result
        let submittedRow = roundsTable.rows[row];
        let submittedCell = submittedRow.cells[1];
        submittedCell.innerHTML = resultString;

        // Button for editing the result
        let button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", "edit");
        button.setAttribute("id", "editResult" + row.toString());
        button.setAttribute("onclick", "tournament.editResult(" + row.toString() + ")");
        button.setAttribute("style", "margin-left: 20px;");
        submittedCell.appendChild(button);

        // Store result for the match
        this.currentRound.pairings[row].result = resultValue;

        // Increment concluded Matches of this round
        this.currentRound.concludedMatches++;

        // Check if this round is over
        if(this.currentRound.concludedMatches === this.currentRound.pairings.length) {
            this.roundsPlayed++;

            // Check if tournament is over
            let button = document.createElement("input");
            button.setAttribute("type", "button");
            if(this.roundsPlayed === this.maxRounds) {
                button.setAttribute("value", "Finalize Tournament");
                button.setAttribute("id", "endTournament");
                button.setAttribute("onclick", "tournament.end()");
                roundsDiv.appendChild(button);
            }
            else {
                button.setAttribute("value", "Start next round");
                button.setAttribute("id", "newRoundButton");
                button.setAttribute("onclick", "tournament.nextRound()");
                roundsDiv.appendChild(button);
            }
            button.scrollIntoView();
        }
    }

    createCheckboxForm(rowNum, pairingHasBye, parentCell, callback) {
        let checkboxes = new Array(7);
        let labels = new Array(7);
        let resultForm = document.createElement("form");
        resultForm.setAttribute("name", "resultForm" + rowNum.toString());

        for(let j = 0; j < 7; j++) {
            labels[j] = document.createElement("label");
            labels[j].setAttribute("for", "resultCheckbox" + rowNum.toString() + j.toString());
            labels[j].innerHTML = results[j];
            checkboxes[j] = document.createElement("input");
            checkboxes[j].setAttribute("type", "radio");
            checkboxes[j].setAttribute("name", "resultCheckbox" + rowNum.toString());
            checkboxes[j].setAttribute("id", "resultCheckbox" + rowNum.toString() + j.toString());
            checkboxes[j].setAttribute("value", j.toString());
            if (j === 0 && pairingHasBye) {
                checkboxes[j].setAttribute("checked", "checked");
            }
            else if(j === 3 && !pairingHasBye) {
                checkboxes[j].setAttribute("checked", "checked");
            }

            labels[j].setAttribute("style", "margin-left: 10px;");
            checkboxes[j].setAttribute("style", "margin-right: 10px;");
            resultForm.appendChild(labels[j]);
            resultForm.appendChild(checkboxes[j]);
        }

        let button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", "submit");
        button.setAttribute("onclick", "tournament.processResult(" + rowNum.toString() +")");
        resultForm.appendChild(button);
        parentCell.appendChild(resultForm);

        if(typeof callback === "function") {
          callback(rowNum);
        }
    }

    // TODO:
    // 1. Set cell content to the form it had earlier
    // 2. concluded matches--
    // 3.
    editResult(rowNum) {

        if(this.currentRound.concludedMatches === this.currentRound.numPairings) {
            if(this.roundsPlayed === this.maxRounds) {
                removeHtmlElement("endTournament");
            }
            else {
                removeHtmlElement("newRoundButton");
            }
            this.roundsPlayed--;
        }

        // Decrement concluded Matches of this round
        this.currentRound.concludedMatches --;

        let row = roundsTable.rows[rowNum];
        let resultCell = row.cells[1];
        resultCell.innerHTML = "";
        this.createCheckboxForm(rowNum, false, resultCell, null);


    }


    /*
     * TODO:
     *  4.4. store tournament
     */
    nextRound() {

        // 1. Change appearance
        let roundsHeader = document.getElementById("roundsHeader");
        roundsHeader.innerHTML = "Round " + (this.roundsPlayed+1).toString();
        removeHtmlElement("newRoundButton");

        // 2. Evaluate Round
        this.evaluateRound();

        // 3. Update standings
        this.printRanking(this.players);

        // 4. New round
        this.currentRound = new Round(this.roundsPlayed, this.maxRounds, this.players);

        // 5. Play Round
        this.playRound();

    }

    end() {
        // 1. Header umbenennen
        document.getElementById("teilnehmerHeader").innerHTML = "Results";

        // 2. Rounds Div löschen
        removeHtmlElement("roundsDiv");

        // 3. Evaluate Round
        this.evaluateRound();

        // 4. Update standings
        this.printRanking(this.players);

        // 5. Neues Leaderboard abfragen
        printLeaderboard(comparatorMemberStats_Elo);

        // 6. Turniersieg vermerken
        incrementTournamentWins(this.players[0].id);

    }

    evaluateRound() {
        // 1. Evaluate the last round
        for(let i = 0; i < this.currentRound.numPairings; i++) {
            let pairing = this.currentRound.pairings[i];

            // Free win
            if(pairing.player2 === byePlayer) {
                this.players[pairing.player1.rank - 1].hadBye = true;
                this.updatePlayersWithWin(pairing.player1.rank, pairing.player2.rank, 0, true);
            }
            else {
                let hasWinner = pairing.result != 3;

                // Check if the game has a clear winner
                if (hasWinner) {
                    if (pairing.result < 3) {
                        this.updatePlayersWithWin(pairing.player1.rank, pairing.player2.rank, pairing.result, false);
                    }
                    else {
                        this.updatePlayersWithWin(pairing.player2.rank, pairing.player1.rank, pairing.result, false);
                    }
                }
                else {
                    this.updatePlayersWithDraw(pairing.player1.rank, pairing.player2.rank);
                }
            }
        }

        // 2. Update Tie-breakers
        for(let i = 0; i < this.players.length; i++) {
            let player = this.players[i];
            let realEnemiesPlayed = this.roundsPlayed;

            if (player.hadBye && realEnemiesPlayed !== 1)
                realEnemiesPlayed -= 1;

            // Opponent Match Win Percentage
            let opponentMatchWinPercentage = 0;
            // Game Win Percentage
            let playerGameWinPercentage = player.gameWins / (player.gameWins + player.gameLoses);
            // Opponent Game Win Percentage
            let opponentGameWinPercentage = 0;

            for(let j = 0; j < this.players.length; j++) {
                let opponent = this.players[j];
                if(!isPossibleMatchUp(player, opponent)) {

                    // Minimum Win% is 0.33
                    opponentMatchWinPercentage += Math.max(opponent.points / (3 * this.roundsPlayed), 0.33);

                    //console.log(player.name + "  opp: " + opponent.name + " oppGameWIns: " + opponent.gameWins + "oppGameLoses: " + opponent.gameLoses);
                    opponentGameWinPercentage += Math.max(opponent.gameWins / (opponent.gameWins + opponent.gameLoses), 0.33);
                    //console.log(opponentGameWinPercentage);
                }
            }

            player.tb1 = Math.round(opponentMatchWinPercentage * 100 / realEnemiesPlayed) / 100;
            player.tb2 = Math. round(playerGameWinPercentage * 100) / 100;
            player.tb3 = Math.round(opponentGameWinPercentage * 100 / realEnemiesPlayed) / 100;

            this.players[i] = player;
        }

        // 3. Sort Players
        this.players.sort(this.playerComparator);

        // 4. Set new ranks
        for(let i = 0; i < this.players.length; i++)
            this.players[i].rank = i+1;

    }

    updatePlayersWithWin(winnerRank, loserRank, result, withBye) {
        // Get winning player
        let winner = this.players[winnerRank-1];
        winner.numWins++;
        winner.points += 3;

        // Update Loser if it wasnt a bye-win
        if(!withBye) {
            let loser = this.players[loserRank - 1];
            loser.numLoses++;

            // Calculate Elo
            let newElos = this.calculateElo(winner.elo, loser.elo, true);
            winner.elo = newElos[0];
            loser.elo = newElos[1];

            // 2:0 or 0:2
            if (result == 0 || result == 6) {
                winner.gameWins += 2;
                loser.gameLoses += 2;
            }
            // 2:1 or 1:2
            else if (result == 1 || result == 5) {
                winner.gameWins += 2;
                winner.gameLoses += 1;
                loser.gameWins += 1;
                loser.gameLoses += 2;
            }
            // 1:0 or 0:1
            else if (result == 2 || result == 4) {
                winner.gameWins += 1;
                loser.gameLoses += 1;

            }

            // Save loser updates only if it wasn't the bye
            this.players[loserRank - 1] = loser;
            updatePlayer(loser.id, loser.elo, 0, 0, 1);

        } else {
            winner.gameWins += 2;
        }

        // Save winner updates in each case, elo has not changed if it was a bye-win
        this.players[winnerRank-1] = winner;
        updatePlayer(winner.id, winner.elo, 1, 0, 0);
    }

    updatePlayersWithDraw(player1Rank, player2Rank) {
        let player1 = this.players[player1Rank-1];
        let player2 = this.players[player2Rank-1];

        player1.numDraws++;
        player1.points += 1;
        player2.numDraws++;
        player2.points += 1;

        let newElos = this.calculateElo(player1.elo, player2.elo, false);

        player1.elo = newElos[0];
        player2.elo = newElos[1];

        player1.gameWins += 1;
        player2.gameWins += 1;
        player1.gameLoses += 1;
        player2.gameLoses += 1;

        this.players[player1Rank - 1] = player1;
        this.players[player2Rank - 1] = player2;

        updatePlayer(player1.id, player1.elo, 0, 1, 0);
        updatePlayer(player2.id, player2.elo, 0, 1, 0);
    }

    calculateElo(winnerElo, loserElo, hadWinner){
        let erwartungswertGewinner = 1 / (1 +  Math.pow(10, (loserElo - winnerElo)/400));
        let erwartungswertVerlierer = 1 / (1 +  Math.pow(10, (winnerElo - loserElo)/400));

        let scoreFactor = 1;
        if(!hadWinner)
            scoreFactor = 0.5;

        let newWinnerElo = Math.round(winnerElo + eloFactor * (scoreFactor - erwartungswertGewinner));
        let newLoserElo = Math.round(loserElo + eloFactor * ( (1-scoreFactor) - erwartungswertVerlierer));

        return [newWinnerElo, newLoserElo];
    }

    playerComparator(a, b) {
        // Points
        if(a.points > b.points) {
            return -1;
        } else if(a.points < b.points) {
            return 1;
        }
        // TB1
        else if (a.tb1 > b.tb1) {
            return -1;
        } else if(a.tb1 < b.tb1) {
            return 1;
        }
        // TB2
        else if(a.tb2 > b.tb2) {
            return -1
        } else if(a.tb2 < b.tb2) {
            return 1;
        }
        // TB§
        else if(a.tb3 > b.tb3) {
            return -1;
        } else if(a.tb3 < b.tb3) {
            return 1
        }
        else {
            return 0;
        }
    }
}

class Player {
    constructor(number, id, name, elo, rank) {
        this.number = number;
        this.playersPlayed = 0;
        this.id = id;
        this.name = name;
        this.elo = elo;
        this.points = 0;
        this.tb1 = 0;
        this.tb2 = 0;
        this.tb3 = 0;
        this.rank = rank;
        this.numWins = 0;
        this.numDraws = 0;
        this.numLoses = 0;
        this.gameWins = 0;
        this.gameLoses = 0;
        this.hadBye = false;
    }
}

class Round {
    constructor(num, maxrounds, players) {
        this.num = num;
        this.maxrounds = maxrounds;
        this.numPairings = Math.ceil(players.length/2);
        this.pairings = this.pair(players);
        this.concludedMatches = 0;

    }

    /* TODO:
     * Nicht optimaler Pairing algorithmus?
     */
    pair(players) {
        class PossibleMatch {
            constructor(p1, p2, cost) {
                this.p1 = p1;
                this.p2 = p2;
                this.cost = cost;
            }
        }

        // Create all possible match ups
        let possibleMatches = [];
        for(let i = 0; i < players.length; i++) {
            let p1 = players[i];
            for(let j = i + 1; j < players.length; j++) {
                let p2 = players[j];
                if(isPossibleMatchUp(p1, p2)) {
                    possibleMatches.push(new PossibleMatch(p1, p2, this.pairingCost(p1, p2)))
                }
            }
            if (players.length%2 === 1) {
                if(!p1.hadBye) {
                    possibleMatches.push(new PossibleMatch(p1, byePlayer, this.pairingCost(p1, byePlayer)));
                }
            }
        }

        // Chose Match-ups from possible ones
        let chosenMatches = [];
        let lastChosenIndex = 0;

        // As long as there are no successfull pairings try again
        while(chosenMatches.length === 0) {
            console.log("Possible Matches: " + possibleMatches.length);

            let pairedPlayers = new Array(players.length + players.length%2);
            pairedPlayers.fill(false);
            // Select from all possible ones only if both players havent been selected yet
            for(let i = 0; i < possibleMatches.length; i++) {
                let p1 = possibleMatches[i].p1;
                let p2 = possibleMatches[i].p2;
                if(pairedPlayers[p1.number] === false && pairedPlayers[p2.number] === false) {
                    pairedPlayers[p1.number] = true;
                    pairedPlayers[p2.number] = true;
                    chosenMatches.push(new Match(p1, p2));
                    lastChosenIndex = i;
                }
            }
            if(chosenMatches.length === Math.ceil(players.length/2)) {
                console.log("Pairings found!");
            }
            else {
                console.log("No pairings found. Trying again...");
                // TODO: Is deleting really the right choice?
                possibleMatches.splice(lastChosenIndex, 1);
                chosenMatches = [];
                lastChosenIndex = 0;
            }
        }

        for(let i = 0; i < chosenMatches.length; i++) {
            let match = chosenMatches[i];
            match.player1.playersPlayed += 1 << match.player2.number;
            match.player2.playersPlayed += 1 << match.player1.number;
        }
        return chosenMatches;
    }

    // Not used atm
    pairingCost(p1, p2) {
        return Math.abs(p1.points - p2.points);
    }

}

class Match {
    constructor(player1, player2) {
        this.player1 = player1;
        this.player2 = player2;

        /*
         * 0: 2-0
         * 1: 2-1
         * 2: 1-0
         * 3: Draw
         * 4: 0-1
         * 5: 1-2
         * 6: 0-2
         */
        this.result = 0;
    }
}

function isPossibleMatchUp(p1, p2) {
    return (p1.playersPlayed & 1 << p2.number) === 0;
}

function getSelectedCheckbox(checkBoxName) {
    let elements = document.getElementsByName(checkBoxName);

    for (let i=0, len=elements.length; i<len; ++i) {
        if (elements[i].checked) {
            elements[i].checked = false;
            return elements[i].value;
        }
    }
}

function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

function getDate() {
    let today = new Date();
    return today.getDate().toString() + '.' + (today.getMonth()+1).toString() + '.' + today.getFullYear().toString()
}