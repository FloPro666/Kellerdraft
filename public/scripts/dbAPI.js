// Initialize Firebase
const config = {
    apiKey: "AIzaSyB51vXh4t9edSouttp7-z3089EnjBqFHDk",
    authDomain: "test-30800.firebaseapp.com",
    databaseURL: "https://test-30800.firebaseio.com",
    projectId: "test-30800"
    // Not needed i think:
    // storageBucket: "test-30800.appspot.com",
    // messagingSenderId: "767094048416"
};
console.log("Initializing database.");
firebase.initializeApp(config);

const db = firebase.database();
const membersDBstr = "members2";
const membersDB = db.ref(membersDBstr);
const namesDBstr = "names2";
const namesDB = db.ref(namesDBstr)
const numMembersDBstr = "numMembers2"
const numMembersDB = db.ref(numMembersDBstr);
let databaseMembers = 0;
let nameToIdMap = new Map();
let idToNameMap = new Map();
let idToEloMap = new Map();
let idToParticipations = new Map();

numMembersDB.once('value', function(snapshot) {
    databaseMembers = snapshot.child("numMembers").val();
    //console.log("Current nummber of members in database: " + databaseMembers);

    //addParticipant("i B");

    //updatePlayer("Andi B", 3000, 0, 0, 1);

});
// Variable for accessing the Tournament section of the database
//var tournamentDB = firebase.database().ref('tournaments');

/*
 * This function is called when a participant is added to a tournament. If the name
 * is new, a new player will be added to the database. The return value is the
 * player's unique id. It can be used for accessing player specific information
 * like the Elo or Name.
 * @param:
 *      username: the player's name
 * @return:
 *      the player's unique id if success, 0 otherwise
 */
function addParticipant(username, callback) {

    if(username === null || username === "") {
        alert("Enter correct name.");
        return 0;
    }

    if(nameToIdMap.has(username)) {
        alert(username + " is already participating in this tournament.");
        return 0;
    }

    // Check if this player is already known
    namesDB.child(username).once('value', function(snapshot) {

        let id = 0;

        // New player
        if(snapshot.val()  === null) {
            if(!confirm("Do you really want to create a new player with name " + username + " (Elo: 1600)?")) {
                return 0;
            }
            console.log("Adding new member > " + username + " < to database/tournament.");
            // When newMember is done, it calls the given callback function to set the input to the maps
            return newMember(username, function(id) {
                idToEloMap.set(id, 1600);
                idToNameMap.set(id, username);
                nameToIdMap.set(username, id);
                idToParticipations.set(id, 0);
                return callback(id);
            });
        }
        // Player known
        else {
            id = snapshot.child("id").val();
            db.ref(membersDBstr + '/' + id + "/elo").once('value', function(snapshot) {
                idToEloMap.set(id, snapshot.val());
            });
            db.ref(membersDBstr + '/' + id + "/participations").once('value', function(snapshot) {
                idToParticipations.set(id, snapshot.val());
            });
            console.log("Adding old member > " + username + " < to tournament.");
            idToNameMap.set(id, username);
            nameToIdMap.set(username, id);
            if(typeof callback === 'function'){
                return callback(id);
            }
        }
    });
}

/*
 * This function can be used to get a player's elo.
 * THIS WORKS ONLY FOR THE PLAYERS IN A TOURNAMENT SO FAR. NOT FOR ALL FROM DATABASE!!!
 * @param:
 *      player: either the player's id or name
 * @return:
 *      The player's elo in case of success, null otherwise
 */
function getElo(player, callback) {
    let id = null;
    if(typeof player === "string") {
        id = getIdFromNameThisTournament(player);
        if(id === null) {
            return null;
        }
    }
    else {
        id = player;
        if(!idToEloMap.has(id)) {
            db.ref(membersDBstr + '/' + id + '/elo').once('value', function(snapshot) {
                let elo = snapshot.val();
                if(elo === null) {
                    console.error("No player known with id: " + id + "  --> Can't get ELO");
                    return null;
                }
                else {
                    return callback(elo);
                }
            });
        }
        else {
            return callback(idToEloMap.get(id));
        }
    }

}

/*
 * This function can be used to update a players scorings in the database.
 * @param:
 *      player: either the player's id or name
 *      newElo: the new Elo of the player (not the difference)
 *      numWinsPlayed: the player's number of wins since the last update
 *      numDrawsPlayed: the player's number of draws since the last update
 *      numLossesPlayed: the player's number of losses since the last update
 * @return:
 *      0 in case of success, null if the player is not participating in this tournament
 */
function updatePlayer(player, newElo, numWinsPlayed, numDrawsPlayed, numLossesPlayed) {

    let id = null;

    if(typeof player === "string") {
        id = getIdFromNameThisTournament(player);
        if(id === null) {
            return;
        }
    }
    else {
        id = player;
        if(!idToNameMap.has(id)) {
            console.error("No participant in the current tournament with name: " + username + "  --> Cannot update.");
            return null;
        }
    }

    db.ref(membersDBstr + '/' + id).once('value', function(snapshot) {
        let updates = {};
        updates[membersDBstr + '/' + id + '/elo'] = newElo;
        updates[membersDBstr + '/' + id + '/negelo'] = -newElo;
        updates[membersDBstr + '/' + id + '/numWins'] = snapshot.child("numWins").val() + numWinsPlayed;
        updates[membersDBstr + '/' + id + '/numDraws'] = snapshot.child("numDraws").val() + numDrawsPlayed;
        updates[membersDBstr + '/' + id + '/numLosses'] = snapshot.child("numLosses").val() + numLossesPlayed;
        updates[membersDBstr + '/' + id + '/pointsOverall'] = snapshot.child("pointsOverall").val() + numWinsPlayed * 3 + numDrawsPlayed;
        updates[membersDBstr + '/' + id + '/roundsPlayed'] = snapshot.child("roundsPlayed").val() + numWinsPlayed + numDrawsPlayed + numLossesPlayed;
        db.ref().update(updates);
        return 0;
    });
}

function incrementParticipations(participants) {
        let updates = {};
        for(let i = 0; i < participants.length; i++) {
            updates[membersDBstr + '/' + participants[i] + '/participations'] = idToParticipations.get(participants[i]) + 1;
        }
        db.ref().update(updates);
}

function incrementTournamentWins(winner) {
    let id = null;
    if(typeof winner === "string") {
        id = getIdFromNameThisTournament(winner);
        if(id === null) {
            return null;
        }
    }
    else {
        id = winner;
        db.ref(membersDBstr + '/' + id + '/tournamentWins').once('value', function(snapshot) {
                let tournamentWins = snapshot.val();
                let updates = {};
                updates[membersDBstr + '/' + id + '/tournamentWins'] = tournamentWins + 1;
                db.ref().update(updates);
        });
    }
}

/*
 * Retrieves the name of the player with the given id from this tournament.
 * @param:
 *      id: the player's id
 * @return:
 *      the corresponding name or null if the player is not in the tournament
 */
function getNameFromIdThisTournament(id) {

    if(idToNameMap.has(id)) {
        return idToNameMap.get(id);
    }
    else {
        console.error("No participant in the current tournament with id: " + id);
        return null;
    }
}

/*
 * Retrieves the id of the player with the given name from this tournament.
 * @param:
 *      username: the player's name
 * @return:
 *      the corresponding id or null if the player is not in the tournament
 */
function getIdFromNameThisTournament(username) {

    if(nameToIdMap.has(username)) {
        return nameToIdMap.get(username);
    }
    else {
        console.error("No participant in the current tournament with name: " + username);
        return null;
    }
}

/*
 * USE ADD PARTICIPANT TO ADD (NEW) MEMBERS TO A TOURNAMENT. THIS FUNCTION IS ONLY USED INTERNALLY.
 * This function creates a new entry in the database for the specified name. In Addition
 * the global field numplayers is used for the player's id and incremented afterwards
 * @param:
 *      username: the player's name
 * @return:
 *      the player's unique id
 */
function newMember(username, callback) {
    // Increment ID
    databaseMembers++;
    // Insert new member with id and stats
    console.log(username + " gets id: " + databaseMembers);
    membersDB.child(databaseMembers.toString()).set({
        id: databaseMembers,
        name: username,
        elo: 1600,
        negelo: -1600,
        numWins: 0,
        numDraws: 0,
        numLosses: 0,
        roundsPlayed: 0,
        participations: 0,
        pointsOverall:0 ,
        avgPointsPerTournament: 0,
        tournamentWins: 0
    });
    // Insert new mapping name-to-id
    namesDB.child(username).set({
        id: databaseMembers
    });
    // Update numMembers field
    numMembersDB.set({
        numMembers: databaseMembers
    });

    return callback(databaseMembers);
}

// Deprecated
function updateRating(username, rating) {
    let updates = {};
    updates[membersDBstr + '/' + username + '/rating'] = rating;
    updates[membersDBstr + '/' + username + '/negrating'] = -rating;
    firebase.database().ref().update(updates);
}

// Deprecated
function getMembers(callback) {
    let memberArray = [];

    let members = firebase.database().ref(membersDBstr).orderByChild('negrating');
    members.once('value', function(snapshot) {
        snapshot.forEach(function(data) {
            let memberArrayEntry = new MemberStatistic(data.child('name').val(), data.child('elo').val(), data.child('pointsOverall').val(),
                                                        data.child('participations').val(), data.child('tournamentWins').val(),
                                                        data.child('numWins').val(), data.child('numDraws').val(), data.child('numLosses').val());
            memberArray.push(memberArrayEntry);
        });

        return callback(memberArray);
    });


}

// Helper function for debugging
function printMap(map) {
    function logMapElements(value, key, map) {
        console.log(value, key,  map);
    }

    map.forEach(logMapElements);
}

class MemberStatistic {
    constructor(name, elo, overallPoints, participations, tournamentWins, wins, draws, loses) {
        this.name = name;
        this.elo = elo;
        this.overallPoints = overallPoints;
        this.participations = participations;
        this.tournamentWins = tournamentWins;
        this.wins = wins;
        this.draws = draws;
        this.loses = loses;
    }
}