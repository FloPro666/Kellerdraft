let memberTable = null;
let anzahl = 0;
let tournament = null;
let leaderboardTable = null;
let memberStatistics = null;
let recentlyUsedComparator = null;

/*
 * This function is called when a new participant for a tournament is added through the form.
 * First it validates the given input. Then it calls addParticipant with the name as string.
 */
function checkNewPlayer() {


    // Check Name input
    let name = document.addParticipantsForm.spielername.value;
    if (name === "" || name === null) {
        document.getElementById("spielernameSpan").innerHTML = "Kein gültiger Name.";
        document.addParticipantsForm.spielername.focus();
        document.getElementById("spielername").style.border = "2px solid red";
        return false;
    }
    else {
        document.getElementById("spielernameSpan").innerHTML = "";
        document.getElementById("spielername").style.border = "2px solid green";
    }

    // Add participant, before returning a callback funtion calls getElo for the created id and increments the
    // number of participants. getElo uses a callback function as well to add the new player with its elo to the
    // visual table
    addParticipant(name, function(id) {
        if(id !== 0) {
            anzahl++;
            getElo(id, function(elo) {
                appendRow(name, elo);
            });
        }
    });
    document.addParticipantsForm.spielername.value = "";
}

function createTable(parentDivName, tableId, headerNameArray, callback) {

    let tableDiv = document.getElementById(parentDivName);
    let table = document.createElement("TABLE");   //TABLE??
    table.setAttribute("id", tableId);
    table.border = '1';
    tableDiv.appendChild(table);

    let header = table.createTHead();

    for(let i = 0; i < headerNameArray.length; i++) {
        let th = table.tHead.appendChild(document.createElement("th"));
        th.innerHTML = headerNameArray[i];
    }

    return table;

}

function createLeaderboardTable(parentDivName, tableId, headerNameArray, callback) {

    let leaderboardTableDiv = document.getElementById(parentDivName);
    leaderboardTableDiv.innerHTML = "";

    let leaderboardTable = document.createElement("TABLE");   //TABLE??
    leaderboardTable.setAttribute("id", tableId);
    leaderboardTable.border = '1';
    leaderboardTableDiv.appendChild(leaderboardTable);

    let header = leaderboardTable.createTHead();

    let th0 = leaderboardTable.tHead.appendChild(document.createElement("th"));
    th0.innerHTML = headerNameArray[0];
    let th1 = leaderboardTable.tHead.appendChild(document.createElement("th"));
    th1.innerHTML = headerNameArray[1];

    for(let i = 2; i < headerNameArray.length; i++) {

        let th = leaderboardTable.tHead.appendChild(document.createElement("th"));
        let button = document.createElement("input");
        button.setAttribute("type", "button");
        button.setAttribute("value", headerNameArray[i]);
        button.setAttribute("onclick", "sortLeaderboard(comparatorMemberStats_" + headerNameArray[i] + ")");
        th.appendChild(button);
    }

    return callback(leaderboardTable);

}

function sortLeaderboard(comparator) {

    memberStatistics = memberStatistics.sort(comparator);
    if(recentlyUsedComparator === comparator) {
        recentlyUsedComparator = null;
    }
    else {
        recentlyUsedComparator = comparator;
    }


    for(let row = 0; row < memberStatistics.length; row++) {

        leaderboardTable.rows[row].cells[1].innerHTML = memberStatistics[row].name;
        leaderboardTable.rows[row].cells[2].innerHTML = memberStatistics[row].participations;
        leaderboardTable.rows[row].cells[3].innerHTML = memberStatistics[row].wins;
        leaderboardTable.rows[row].cells[4].innerHTML = memberStatistics[row].draws;
        leaderboardTable.rows[row].cells[5].innerHTML = memberStatistics[row].loses;
        leaderboardTable.rows[row].cells[6].innerHTML = memberStatistics[row].overallPoints;
        leaderboardTable.rows[row].cells[7].innerHTML = memberStatistics[row].tournamentWins;
        leaderboardTable.rows[row].cells[8].innerHTML = memberStatistics[row].elo;
    }


}

function appendRow(name, elo) {

    if(memberTable === null) {
        console.error("Unable to append row to not-exisiting table");
        return;
    }

    let rowCount = memberTable.rows.length;
    let row = memberTable.insertRow(rowCount);

    row.insertCell(0).innerHTML = anzahl;
    row.insertCell(1).innerHTML = name;
    row.insertCell(2).innerHTML = elo;
}

function startTournament(){
    if(anzahl<1){
        document.getElementById("turniertitelSpan").innerHTML = "Kein Turnier ohne Mitspieler!!";
        return;
    }
    else if(anzahl === 1) {
        document.getElementById("turniertitelSpan").innerHTML = "Willst du alleine spielen?!";
        return;
    }

    // Check Name input
    let name = document.startTournamentForm.turniertitel.value;
    if (name === "" || name === null) {
        document.getElementById("turniertitelSpan").innerHTML = "Kein gültiger Name.";
        document.startTournamentForm.turniertitel.focus();
        document.getElementById("turniertitel").style.border = "2px solid red";
        return;
    }

    if(!confirm("Do you really want to start the new tournament " + name + "?\nNumber of players: " + anzahl +
            "\nNumber of rounds to play: " + Math.ceil(Math.log2(anzahl)).toString() + "\nFormat: Bo3")) {
        return;
    }

    console.log("Starting new tournament.");

    // Change appearance
    document.getElementById("turnierHeader").innerHTML = name;
    document.getElementById("teilnehmerHeader").innerHTML = "Standings";
    removeHtmlElement("addParticipantsDiv");
    removeHtmlElement("startTournamentDiv");

    tournament = new Tournament(name, anzahl);
}

function printLeaderboard(comparator) {
    getMembers(function(memberArray) {
        memberStatistics = memberArray;

        leaderboardTable = createLeaderboardTable("leaderBoardDiv", "leaderboardTable", ["Rank", "Name", "Participations", "Wins", "Draws", "Loses", "Points_Overall", "Tournament_Wins", "Elo"],
            function(table) {
                memberArray = memberArray.sort(comparator);

                for(let i = 0; i < memberArray.length; i++) {
                    let rowCount = table.rows.length;
                    let row = table.insertRow(rowCount);
                    row.insertCell(0).innerHTML = (i+1).toString() + '.';
                    row.insertCell(1).innerHTML = memberArray[i].name;
                    row.insertCell(2).innerHTML = memberArray[i].participations;
                    row.insertCell(3).innerHTML = memberArray[i].wins;
                    row.insertCell(4).innerHTML = memberArray[i].draws;
                    row.insertCell(5).innerHTML = memberArray[i].loses;
                    row.insertCell(6).innerHTML = memberArray[i].overallPoints;
                    row.insertCell(7).innerHTML = memberArray[i].tournamentWins;
                    row.insertCell(8).innerHTML = memberArray[i].elo;
                }
                let headerChildren = table.tHead.children;
                table.tHead.children[0] = document.createElement("input");

                return table;
        });



    })
}

// Comparator #0
function comparatorMemberStats_Elo(a, b) {

    let c1 = -1;
    let c2 = 1;

    if(recentlyUsedComparator === comparatorMemberStats_Elo) {
        c1 = 1;
        c2 = -1;
    }

    if(a.elo > b.elo)
        return c1;
    if(a.elo < b.elo)
        return c2;
    return 0;
}

// Comparator #1
function comparatorMemberStats_Participations(a, b) {
    let c1 = -1;
    let c2 = 1;

    if(recentlyUsedComparator === comparatorMemberStats_Participations) {
        c1 = 1;
        c2 = -1;
    }

    if(a.participations > b.participations)
        return c1;
    if(a.participations < b.participations)
        return c2;
    return 0;
}

// Comparator #2
function comparatorMemberStats_Wins(a, b) {
    let c1 = -1;
    let c2 = 1;

    if(recentlyUsedComparator === comparatorMemberStats_Wins) {
        c1 = 1;
        c2 = -1;
    }

    if(a.wins > b.wins)
        return c1;
    if(a.wins < b.wins)
        return c2;
    return 0;
}

// Comparator #3
function comparatorMemberStats_Draws(a, b) {
    let c1 = -1;
    let c2 = 1;

    if(recentlyUsedComparator === comparatorMemberStats_Draws) {
        c1 = 1;
        c2 = -1;
    }

    if(a.draws > b.draws)
        return c1;
    if(a.draws < b.draws)
        return c2;
    return 0;
}

// Comparator #4
function comparatorMemberStats_Loses(a, b) {
    let c1 = -1;
    let c2 = 1;

    if(recentlyUsedComparator === comparatorMemberStats_Loses) {
        c1 = 1;
        c2 = -1;
    }

    if(a.loses > b.loses)
        return c1;
    if(a.loses < b.loses)
        return c2;
    return 0;
}

// Comparator #5
function comparatorMemberStats_Points_Overall(a, b) {
    let c1 = -1;
    let c2 = 1;

    if(recentlyUsedComparator === comparatorMemberStats_Points_Overall) {
        c1 = 1;
        c2 = -1;
    }

    if(a.overallPoints > b.overallPoints)
        return c1;
    if(a.overallPoints < b.overallPoints)
        return c2;
    return 0;
}

// Comparator #6
function comparatorMemberStats_Tournament_Wins(a, b) {
    let c1 = -1;
    let c2 = 1;

    if(recentlyUsedComparator === comparatorMemberStats_Tournament_Wins) {
        c1 = 1;
        c2 = -1;
    }

    if(a.tournamentWins > b.tournamentWins)
        return c1;
    if(a.tournamentWins < b.tournamentWins)
        return c2;
    return 0;
}

function removeHtmlElement(htmlId) {
    let elem = document.getElementById(htmlId);
    elem.parentNode.removeChild(elem);
}