class Spieler{
	constructor(name){
		this.name = name;
		this.elo = 0;
		this.id = 0;
	}
}

class Tabelleneintrag {
    constructor(ID){
        this.ID = ID;
		this.Rang = ID+1;
        this.Name = '';
        this.Punkte = 0;
		this.PlayedHash = 0;
        console.log(this.ID + ' erstellt.');
    }
	punkten(Anzahl){
		this.Punkte += 2;
		console.log(this.Name + ' hat gepunktet!!!');
	}
}
class Match{
	constructor(){
		this.spieler1 = -1;
		this.spieler2 = -1;
		this.result;
	}
}

class Round{
	constructor(count){
		console.log('Runde erstellt. Count = '+ count);
		this.matchCount = count;
		this.Matches = new Array(count);
		for(var i = 0 ; i < count; i++){
			this.Matches[i] = new Match();
			console.log('Match ' + i + ' erstellt.');
		}
		
	}
}
class Tabelle {
    constructor(Anzahl){
        this.Anzahl = Anzahl;
        this.Eintraege= new Array(Anzahl);
        for (var i = 0; i < anzahl ; i++) {
            this.Eintraege[i] = new Tabelleneintrag(i);
        }
		this.Rounds = Array(Math.ceil(Math.log2(Anzahl)));
		this.gespielteRunden = 0;
		console.log(Math.ceil(Math.log2(Anzahl)) + ' Runden wurden angesetzt.');
    }

    // TODO: gerade wird zu jedem Eintrag der neue name geschrieben, i < this.Anzahl?,
    spielerHinzufuegen(iName){
        var i = 0;
        while(i<=this.Anzahl){
            if(this.Eintraege[i].Name === ""){
                this.Eintraege[i].Name = iName;
                break;
            }
            i = i +1;
        }
        console.log(iName + ' an Stelle ' + i +  ' hinzugefuegt.');

        for(var j = 0; j < this.Anzahl; j++) {
            console.log(this.Eintraege[i]);
        }
    }

	pair(){
		this.sort();
		this.Rounds[this.gespielteRunden] = new Round(Math.ceil(this.Anzahl/2));
		var round = this.Rounds[this.gespielteRunden];
		var paired = new Array(this.Anzahl);
		for(var i = 0 ; i< this.Anzahl; i ++){
			paired[i] = 0;
		}
		for(var i = 0 ; i< Math.ceil(this.Anzahl)/2 ; i++){
			console.log('i = ' + i);
			console.log('Math.ceil(this.Anzahl)/2 = '+ Math.ceil(this.Anzahl)/2);
			var player1ID = 0 ;
			var player2ID = 0;
			for(var rank = 1 ; rank< this.Anzahl+1; rank++){
				var found  = 0;
				for(var ii = 0 ; ii<this.Anzahl; ii++){
					if((this.Eintraege[ii].Rang!=(rank)) ||(paired[ii]!=0)){continue;}
					console.log('Untersuchter Spieler: ' + this.Eintraege[ii].Name + ' Rang: ' + this.Eintraege[ii].Rang);
					round.Matches[i].spieler1 = this.Eintraege[ii].ID;
					player1ID = ii;
					paired[ii]=1;
					console.log('Spieler 1: '+ this.Eintraege[ii].Name);
					found = 1;
					break;	
				}
				if(found){break;}
			}
			for(var rank = 1 ; rank< this.Anzahl+1; rank++){
				for(var ii = 0 ; ii< this.Anzahl; ii++){	
					var found = 0;
					//console.log('Untersuchter Spieler: ' + this.Eintraege[ii].Name + ' Rang: ' + this.Eintraege[ii].Rang);
					if((this.Eintraege[ii].Rang!=(rank)) ||(paired[ii]!=0)){continue;}
					if(this.Eintraege[ii].PlayedHash & (1<<(player1ID))){continue;}
					round.Matches[i].spieler2 = this.Eintraege[ii].ID;
					paired[ii]=1;
					var player2ID= ii;	
					console.log('Spieler 2: '+ this.Eintraege[ii].Name);
					found =  1;
					break;
				}
				if(found){break;}
			}			
			this.Eintraege[player1ID].PlayedHash |= 1<<this.Eintraege[player2ID].ID;		
			this.Eintraege[player2ID].PlayedHash |= 1<<this.Eintraege[player1ID].ID;
			console.log('player1ID: ' + player1ID + ', player2ID: ' + player2ID);
			console.log('round.Matches[i].spieler1: ' + round.Matches[i].spieler1 + ', round.Matches[i].spieler2: ' + round.Matches[i].spieler2);
			var id1 = round.Matches[i].spieler1;
			var id2 = round.Matches[i].spieler2;
			console.log('Spiel '+ i + ': ' + this.Eintraege[id1].Name + ' gegen ' + this.Eintraege[id2].Name);
		}
		
	}
	sort(){
		
		
		var mPoints = -1;
		var spID = -1;
		var tEintrag = this.Eintraege;
		for(var i = 0 ; i <this.Anzahl; i++){
			this.Eintraege[i].Rang = -1;
		}
		for(i = 0 ; i <this.Anzahl; i++){ //Nach jedem Rang einzeln Suchen		
			//console.log('Suche nach Platz '+ (i+1));
			
			for(var ii = 0 ; ii < this.Anzahl; ii++){ //Die Liste durchsuchen
				if(this.Eintraege[ii].Punkte > mPoints){
					//console.log(this.Eintraege[ii].Name + ', Punkte: '+ this.Eintraege[ii].Punkte);
					if((i==0) || (this.Eintraege[ii].Rang == -1)){
						mPoints =  this.Eintraege[ii].Punkte;
						spID = ii;
					}
				}
			}
			
			//console.log(this.Eintraege[spID].Name + ' jetzt auf Rang ' + (i+1));
			this.Eintraege[spID].Rang = i+1;
			mPoints = -1;
			spID = -1;
		}
	}
    print(Ziel){

        var element, newElement, parent;
        // Get the original element
        element = document.getElementById(Ziel);

        // Assuming it exists...
        if (element) {
            // Get its parent
            parent = element.parentNode;

            // Create the new element
            newElement = document.createElement('div');

            // Insert the new one in front of the old one (this temporarily
            // creates an invalid DOM tree [two elements with the same ID],
            // but that's harmless because we're about to fix that).
            parent.insertBefore(newElement, element);

            // Remove the original
            parent.removeChild(element);
        }
        console.log('Printe Tabelle.');
        memberTable = document.createElement("TABLE");   //TABLE??
        table.setAttribute("Rang", "Spieler", "Punkte");
        table.border = '1';
        newElement.appendChild(table);
        var header = table.createTHead();

        var th0 = table.tHead.appendChild(document.createElement("th"));
        th0.innerHTML = "Rang";
        var th1 = table.tHead.appendChild(document.createElement("th"));
        th1.innerHTML = "Spieler";
        var th2 = table.tHead.appendChild(document.createElement("th"));
        th2.innerHTML = "Punkte";
		for(var i= 0 ; i<this.Anzahl; i++){ //Rang
			for(var ii = 0 ; ii<this.Anzahl; ii++){//Spieler nach Rang durchsuchen
				if(this.Eintraege[ii].Rang == i+1){
					var rowCount = table.rows.length;
					var row = table.insertRow(rowCount);
					row.insertCell(0).innerHTML = i + 1;
					row.insertCell(1).innerHTML = this.Eintraege[ii].Name;
					console.log(this.Eintraege[i].Name);
					row.insertCell(2).innerHTML = this.Eintraege[ii].Punkte;
				}
			}
		}
    }
}