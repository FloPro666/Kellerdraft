# Kellerdraft 

# Prerequisites
## Development
* npm >= 5.x
* nodejs >= 8.11

# Development Setup
## Install
```bash
$ git clone <url-to-this-repository>
$ npm install
```

## Run
```bash
$ npm run start
```